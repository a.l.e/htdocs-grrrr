<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// TODO: remove this in production
// clearstatcache();

if (!function_exists('debug')) {
    function debug($label, $value) {
        // if ($_SERVER['REMOTE_ADDR'] == '178.195.77.72') {
            echo("<pre>$label: ".str_replace(array('<', '>'), array('&lt;', '&gt;'), print_r($value, 1))."</pre>");
            // echo("<pre>".print_r(debug_backtrace(), 1)."</pre>");
        // }
    }
}

define('TILE_CACHE_DIRECTORY', 'cache/');
define('CACHE_TIMEOUT', 86400 * 7); // php cache for 7 days

function get_tile_from_osm($imgname)
{
    $opts = array('http'=>array('header' => "User-Agent:GrrrrNet/1.0\r\n"));
    $context = stream_context_create($opts);
    $string = file_get_contents($imgname, false, $context);
    $im = imagecreatefromstring($string);

    if ($im) {
        return $im;
    }

    return false;
}

function get_error_tile($coordinates) {
    $im  = imagecreatetruecolor(150, 30);
    $bgc = imagecolorallocate($im, 255, 255, 255);
    $tc  = imagecolorallocate($im, 0, 0, 0);
    imagefilledrectangle($im, 0, 0, 150, 30, $bgc);
    imagestring($im, 1, 5, 5, 'Error loading ' . $coordinates, $tc);

    return $im;
}

function get_cache_filename($z, $x, $y, $gray) {
    return TILE_CACHE_DIRECTORY.sprintf('tile_%d_%d_%d_%d', $z, $x, $y, $gray).'.png';
}

function add_tile_to_cache($filename, $image) {
    imagepng($image, $filename);
}

function remove_expired_cache() {
    $fi = new FilesystemIterator(TILE_CACHE_DIRECTORY, FilesystemIterator::SKIP_DOTS);
    $time = time();
    foreach ($fi as $file) {
        if (!$file->isFile() || (time - $file->getMTime() <= CACHE_TIMEOUT)) {
            continue;
        }
        unlink($file);
    }
}

function get_tile_from_cache($z, $x, $y, $gray) {
    $filename = get_cache_filename($z, $x, $y, $gray);
    if (is_file($filename)) {
        $last_modification = filemtime($filename);
        if (time() - $last_modification <= CACHE_TIMEOUT) {
            return [imagecreatefrompng($filename), $last_modification];
        }
    }
    return [false, 0];
}

function get_grayscale($image, $gray) {
    $result = null;
    $width = imagesx($image);
    $height = imagesy($image);
    $image_gray = imagecreatetruecolor($width, $height);
    imagecolorallocate($image_gray, 0, 0, 0);
    for ($i = 0; $i < $width; $i++) {
      for ($j = 0; $j < $height; $j++) {
        $rgb = imagecolorat($image, $i, $j);
        $rgba = imagecolorsforindex($image, $rgb);
        $r = $rgba['red'];
        $g = $rgba['green'];
        $b = $rgba['blue'];
        // debug('rgba', $rgba);
        // in gray mode $r == $g == $b
        $color = floor(($r + $g + $b) * 0.33);
        if ($gray == 0) {
            $color = ($r + $g + $b) * 0.33;
            $gray_color = imagecolorexact($image_gray, $color, $color, $color);
        } elseif ($color > $gray) {
            $gray_color = imagecolorexact($image_gray, 255, 255, 255);
        } else {
            $gray_color = imagecolorexact($image_gray, 0, 0, 0);
        }
        // $gray_color = imagecolorexact($image_gray, $r, $g, $b);
        imagesetpixel($image_gray, $i, $j, $gray_color);
       }
    }
    $result = $image_gray;
    return $result;
}

function get_header_date($time) {
    return gmdate("D, d M Y H:i:s", $time) ." GMT";
}

$s = $_GET['s'];
if ($s != 'a' && $s != 'b' && $s != 'c') {
    $s = 'a';
}
$z = intval($_GET['z']);
$x = intval($_GET['x']);
$y = intval($_GET['y']);
$gray = array_key_exists('g', $_GET) ? intval($_GET['g']) : 200;

if (($z >= 0) && ($x >= 0) && ($y >= 0)) {
    $cache_filename = get_cache_filename($z, $x, $y, $gray);
    list($image_gray, $last_modification) = get_tile_from_cache($z, $x, $y, $gray);
    if ($image_gray === false) {
        // break after 5 attempts
        $attempt = 0;
        $image_gray = null;
        do {
            // debug('url', "http://$s.tile.openstreetmap.org/$z/$x/$y.png");
            $image = get_tile_from_osm("http://$s.tile.openstreetmap.org/$z/$x/$y.png");
            // debug('image', $image);
            if ($image) {
                $image_gray = get_grayscale($image, $gray);
                imagedestroy($image);
            } else {
                $image_gray = $image;
            }
            $attempt++;
        } while ($attempt < 5 && is_null($image_gray));

        if (isset($image_gray)) {
            add_tile_to_cache($cache_filename, $image_gray);
        } else {
            error_log("could not get ($x, $y, $z).");
            $image_gray = get_error_tile("$x, $y, $z");
        }
        $last_modification = time();
    }
} else {
    error_log("invalid (x, y, z): ($x, $y, $z).");
    $image_gray = get_error_tile("$x, $y, $z");
    $last_modification = time();
}

if (rand(0, 10000) == 0) { // about every 10'000 calls clean up the cache
    remove_expired_cache();
}

// debug('image_gray', $image_gray);

if (!headers_sent()) {
    header("Expires: " . get_header_date(time() + CACHE_TIMEOUT));
    header("Last-Modified: " . get_header_date($last_modification));
    header("Cache-Control: public, max-age=" . CACHE_TIMEOUT);
    header('Content-Type: image/png');
    imagepng($image_gray);
    imagedestroy($image_gray);
}
