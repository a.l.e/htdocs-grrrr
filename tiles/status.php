<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

define('TILE_CACHE_DIRECTORY', 'cache/');
define('CACHE_TIMEOUT', 86400 * 7); // php cache for 7 days

function check($label, $f) {
    return '<span class="' . ($f() ? 'pass' : 'fail') .'">' . $label . '</span>';
}

$cache_exists = function() {
    return is_dir(TILE_CACHE_DIRECTORY);
};

$cache_is_writable = function() {
    return is_writable(TILE_CACHE_DIRECTORY);
};

function cache_count_files() {
    $fi = new FilesystemIterator(TILE_CACHE_DIRECTORY, FilesystemIterator::SKIP_DOTS);
    return iterator_count($fi);
};

function get_cache_size() {
    $fi = new FilesystemIterator(TILE_CACHE_DIRECTORY, FilesystemIterator::SKIP_DOTS);
    $bytes = array_reduce(iterator_to_array($fi), function($a, $e) {
        return $a + $e->getSize();
    }, 0);
    if ($bytes >= 1048576) {
        return number_format($bytes / 1048576, 2) . ' MB';
    } else {
        return number_format($bytes / 1024, 2) . ' KB';
    }
};

function clear_cache() {
    $fi = new FilesystemIterator(TILE_CACHE_DIRECTORY, FilesystemIterator::SKIP_DOTS);
    foreach ($fi as $file) {
        if (!$file->isFile()) {
            continue;
        }
        unlink($file);
    }
}

if (array_key_exists('clear_cache', $_GET)) {
    clear_cache();
    header('location: '.$_SERVER['SCRIPT_NAME']);
}

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <style>
  .pass {
      padding: 4px;
      font-weight: bold;
      color: white;
      background-color: darkgreen;
  }
  .fail {
      padding: 4px;
      font-weight: bold;
      color: white;
      background-color: darkred;
  }
  </style>
</head>
<body>
<ul>
    <li>Cache directory <?= check('exists', $cache_exists) ?> and <?= check('is writable', $cache_is_writable) ?>.</li>
    <li>Cache size: <?= cache_count_files() ?> files using <?= get_cache_size() ?>.</li>
    <li><a href="?clear_cache">Clear cache</a></li>
</ul>
