function show_map(dom_id, coord, zoom) {

  let bild = document.createElement('div');
  bild.id = 'bild';
  bild.style.zIndex = '2';
  bild.onclick = function () {hide(); return false;};
  document.body.appendChild(bild);

  var map = new L.map(dom_id)
    .setView(coord, zoom);

  if ((self.location.href.substr(0, 8) != 'file:///')) {
    var osmUrl = '../tiles/tile.php?s={s}&z={z}&x={x}&y={y}&g=200'; // g = 1 - 254; 0 = graustufe
  } else {
    var osmUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
  }

  var osmAttrib='Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> &mdash; js+html+php <a href="http://ideale.ch">a.l.e</a>';

  osm = new L.tileLayer(osmUrl, {
    maxZoom: 18,
    attribution: osmAttrib}).addTo(map);

  map.on('locationfound', onLocationFound);

  function onLocationFound(e) {
    var ortIcon = L.icon({
      iconUrl: '../grr/grrrr_kreuzblink.gif',
      shadowUrl: '../grr/grrrr_kreuzblink.gif',
      iconSize: [19, 19],
      shadowSize: [0, 0],
      iconAnchor: [10, 10],
      popupAnchor: [-10, -10]
    });
    var marker = L.marker(e.latlng, {icon: ortIcon})
      .addTo(map);
  }

  map.locate(); // will ask the current location...

  // initialise the dots
  var DotIcon = L.Icon.extend({
    options: {
      iconUrl: '../tiles/grrrr_da_h.png',
      shadowUrl: '../tiles/grrrr_da_h.png',
      iconSize: [25, 25],
      shadowSize: [0, 0],
      iconAnchor: [0, 25],
      popupAnchor: [0, 0]
    }
  });

  blinkDot = [
    new DotIcon(),
    new DotIcon({iconUrl: '../tiles/grrrr_da_v.png'}),
  ];
  
  // initialise the markers
  var markers = [];
  if (typeof(markers_global) != "undefined") {
    markers = markers.concat(markers_global);
  }
  if (typeof(markers_local) != "undefined") {
    markers = markers.concat(markers_local);
  }

  // add  the markers
  for (var i=0; i<markers.length; i++) {
    
    var lon = markers[i][0];
    var lat = markers[i][1];
    var url = markers[i][2];
    var grr = markers[i][3];
		var marker;
		if (grr == 0) {
			marker = new L.Marker(new L.LatLng(lat, lon), {icon: ( blinkDot[i%2]) });
    } else if (!((typeof(orte_ignorieren) != "undefined") && (orte_ignorieren.indexOf(grr) != -1))) {
			var width = markers[i][4];
			var grrIcon = L.icon({
        iconUrl: '../grr/grr'+grr+'n.png',
        shadowUrl: '../grr/grr'+grr+'n.png',
        iconSize: [width, 19],
        shadowSize: [0, 0],
        iconAnchor: [width/2, 10],
        popupAnchor: [0, 0]
			});

			marker = L.marker([lat, lon], {icon: grrIcon });
		}
		marker.data = markers[i];
		marker.on('click', markerclick);
    marker.addTo(map);
  }

  // callback for the clicks on the markers
  function markerclick(e) {
		if (e.target.data[3] == 0) {
			show(e.target.data[2]);
		} else {
			document.location = e.target.data[2];
		}
  }
}

// actions for the markers
function show(image) {
  grrimage="url("+image+")";
  document.getElementById('bild').style.backgroundImage=grrimage;
  document.getElementById('bild').style.visibility='visible';
}

function hide() {
  document.getElementById('bild').style.visibility='hidden';
}
