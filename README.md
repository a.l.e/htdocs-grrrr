# grrrr.net

The map subset of the files on <http://grrrr.net>.

# Changes

## 2021-01-11

- Add the user agent in the request to OSM.
- Only use `file_get_contents` for fetching data.
- Try harder to get a tile from OSM (5 tries)
- Correctly set the HTTP headers for the cache.
- Fix the conversion from RGB to grayscale.
- Simplify / improve the cache (partially inspired by <https://wiki.openstreetmap.org/wiki/ProxySimplePHP5>).
- Clean up the `index.html` files and move the CSS and javascript to separate files.
- Upgrade to the current version of Leaflet.
- add a `tiles/status.php/` script that shows the size of the the cache and allows to delete it.
- use the `tiles/` directory instead of `map/`.

### Upgrade path

Edit the `index.html` files:

- add the `DOCTYPE` and the `charset`.
- load `../map/grrrr.css`.
- load `../map/grrrr.js`.
- replace the js with the `window.onload`.
  - the arguments of `show_map` are:
    - the id of the div with the map (normally 'map')
    - a list with the lat and a long of the center of the map.
  - move all the (remaining) _script_ to the end of the body.
- Use multiple lines for defining the logos.
- simplify the `<div id="map"...>`
- remove the `<div id="bild"...>`

Create a a new `tiles/` directory and put in there new code

- `tile.php`
- `grrrr.js`
- `grrrr.css`
- `orte.js`
- `leaflet.js`
- `leaflet.css`
- `leaflet.js.map`
- an empty `cache/`

Cleanup the `map/` directory:

- remove the `tile.php` file
- remove the old `leaflet.js` and `leaflet.css` files
- remove the old `orte.js`
